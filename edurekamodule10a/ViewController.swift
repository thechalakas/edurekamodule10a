//
//  ViewController.swift
//  edurekamodule10a
//
//  Created by Jay on 02/03/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{

    //we will set this label based on the language setting of the phone.
    @IBOutlet var internationalLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //get the string based on region.
        let toDisplayString = NSLocalizedString("lablekey1", comment: "label key will come here")
        internationalLabel.text = toDisplayString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

